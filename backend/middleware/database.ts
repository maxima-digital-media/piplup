import { IDBService } from "../models/service.db";
import { MongoClient } from "mongodb";

export class configureClient {
  private serviceCreds: IDBService;
  private Uri: string = "";

  constructor(serviceCredentials: IDBService) {
    this.serviceCreds = serviceCredentials;
    this.authDatabase();
  }

  // Authicate the service
  private authDatabase(): void {
    const creds = {
      url: this.serviceCreds.clusterUrl,
      serviceId: this.serviceCreds.serviceId,
      accessToken: this.serviceCreds.accessToken,
      authMechanism: this.serviceCreds.authMechanism,
    };

    this.Uri = `mongodb://${creds.serviceId}:${creds.accessToken}@${creds.url}/?authSource=maxima&authMechanism=${creds.authMechanism}`;
  }

  public createClient() {
    const client = new MongoClient(this.Uri, {
      useNewUrlParser: true,
      useUnifiedTopology: true,
    });

    return client;
  }
}

// CRUD Operations
export class queryDatabase {
  private client: MongoClient;

  constructor(dbClient: MongoClient) {
    this.client = dbClient;
  }

  public openConnection() {
    this.client.connect();
  }

  public closeConnection() {
    this.client.close();
  }

  private async prepareCollectionForQuery(
    database: string,
    collection: string
  ) {
    const db = this.client.db(database);
    const col = db.collection(collection);
    return col;
  }

  public async read(db: string, col: string, query: object, options?: object) {
    const qryCol = await this.prepareCollectionForQuery(db, col);
    let qryResult = qryCol.find(query, options);
    let result: any[] = [];

    await qryResult.forEach((doc) => {
      result.push(doc);
    });
    return result;
  }

  public async write(
    db: string,
    col: string,
    query: object[],
    options?: object
  ) {
    const qryCol = await this.prepareCollectionForQuery(db, col);

    let result: number | object;
    result = 0;
    if (query.length > 1) {
      await qryCol.insertMany(query, options).then((res) => {
        result = res.result;
      });
    } else if (query.length === 1) {
      await qryCol.insertOne(query[0], options).then((res) => {
        result = res.result;
      });
    }
    return result;
  }

  public async update(
    db: string,
    col: string,
    query: object,
    filter: object,
    options?: object
  ) {
    const qryCol = await this.prepareCollectionForQuery(db, col);
    let result: number | object;

    result = 0;
    await qryCol
      .updateMany(filter, query, options)
      .then((res) => (result = res.result));

    return result;
  }

  public async delete(
    db: string,
    col: string,
    query: object,
    options?: object
  ) {
    const qryCol = await this.prepareCollectionForQuery(db, col);
    let result: number | object;

    result = 0;
    await qryCol
      .deleteMany(query, options)
      .then((res) => (result = res.result));
    return result;
  }
}
