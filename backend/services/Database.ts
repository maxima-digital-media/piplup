import fs from "fs";
import { cwd } from "process";

import express from "express";
import cors from "cors";
import bodyParser from "body-parser";

import { configureClient, queryDatabase } from "../middleware/database";
import { IDBService } from "../models/service.db";

const service = express();

const rootDir: string = cwd().split("services")[0];
const configFile = fs.readFileSync(`${rootDir}/config/mongo.config`, "utf-8");
const configJson = JSON.parse(configFile);

const credentials: IDBService = {
  clusterUrl: "127.0.0.1",
  serviceId: "10630f792c5d8289fee21ec44051014f51cf2710",
  accessToken: configJson.piplupCreds.accessToken,
  authMechanism: configJson.piplupCreds.authMechanism,
};

const databaseClient = new configureClient(credentials).createClient();
const client = new queryDatabase(databaseClient);
const db = "maxima";
const col = "piplup";
const dbColUri = "/data/maxima/piplup";

client.openConnection();

service.use(cors());
service.use(bodyParser.json());
service.use(bodyParser.urlencoded({ extended: true }));

service.get(`${dbColUri}/:type`, (req, res, next) => {
  client.read(db, col, req.body).then((qRes) => {
    console.log('Received');
    res.json(qRes);
  });
});

/*
[NOTE] : [I] Piplup SHOULD NOT BE ALLOWED to edit the database in any way
[DEPRECATION] : [R] Restrictions will be added | [I] v0.1.2 will no longer have CUD
operations unabled.
*/

service.post(`${dbColUri}/:type`, (req, res, next) => {
  client.write(db, col, req.body).then((qRes) => {
    console.log(qRes);
    res.json(qRes);
  });
});

service.patch(`${dbColUri}/:type`, (req, res, next) => {
  client.update(db, col, req.body.query, req.body.filter).then((qRes) => {
    res.json(qRes);
  });
});

service.delete(`${dbColUri}/:type`, (req, res, next) => {
  client.delete(db, col, req.body).then((qRes) => {
    res.json(qRes);
  });
});

service.listen(8081, () => {
  console.log("Database Service Running on local port 8081");
});
