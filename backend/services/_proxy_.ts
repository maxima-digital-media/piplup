import http from "http";
import { cwd } from "process";
import express from "express";
import cors from "cors";

const router = express();
router.use(cors());

const rootDir: string = cwd().split("services")[0];

router.get("/data/maxima/piplup/:type", (req, res, next) => {
  let headers = req.headers;
  http
    .get(`http://localhost:8081${req.originalUrl}`, (response) => {
      let data = "";
      response.on("data", (chunk) => {
        data += chunk;
      });
      response.on("end", () => {
        try {
          res.json(JSON.parse(data));
        } catch (error) {
          console.error(error.message);
        }
      });
    })
    .on("error", (error) => {
      console.error(error.message);
    });
});

router.get("/media/:type/:filename", (req, res, next) => {
  if (req.params.type === "audio") {
    res.sendFile(`${rootDir}/media/audio/${req.params.filename}`);
  }
  if (req.params.type === "video") {
    res.sendFile(`${rootDir}/media/video/${req.params.filename}`);
  }
  if (req.params.type === "image") {
    res.sendFile(
      `${rootDir}/media/image/${req.params.filename.split(".")[0]}.webp`
    );
  }
});

router.get("/app", (req, res, next) => {
  // http.get("http://localhost:3000");
});

router.listen(8080, () => {
  console.log("Reserve Proxy Running on local port 8080");
});
