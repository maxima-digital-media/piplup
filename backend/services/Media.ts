import { cwd } from "process";

import express from "express";
import cors from "cors";

const service = express();
service.use(cors());

const rootDir: string = cwd().split("services")[0];

service.get("/media/:type/:filename", (req, res) => {
  if (req.params.type === "audio") {
    res.sendFile(`${rootDir}/media/audio/${req.params.filename}`);
  }
  if (req.params.type === "video") {
    res.sendFile(`${rootDir}/media/video/${req.params.filename}`);
  }
  if (req.params.type === "image") {
    res.sendFile(
      `${rootDir}/media/image/${req.params.filename.split(".")[0]}.webp`
    );
  } /*else {
    res.status(404).send("File Not Found");
  }*/
});

service.listen(8083, () => {
  console.log("Media Access Service Running on local port 8083");
});
