type AuthMechanism =
  | "DEFAULT"
  | "SCRAM-SHA-256"
  | "SCRAM-SHA-1"
  | "MONGODB-CR"
  | "X.509";

export interface IDBService {
  clusterUrl: string;
  serviceId: string;
  accessToken: string;
  authMechanism: AuthMechanism;
}
