import { configureClient, queryDatabase } from "../../middleware/database";

import { IDBService } from "../../models/dbService.model";

// Test configureClient.createClient()
const dbService: IDBService = {
  clusterUrl: "127.0.0.1",
  serviceId: "10630f792c5d8289fee21ec44051014f51cf2710",
  accessToken:
    "706bc50d617f44fe45630650fae1adc5262aa3c04ee1f87a62645b03a0d6cac3",
  authMechanism: "SCRAM-SHA-256",
};
const client = new configureClient(dbService).createClient();

// Test queryDatabase modules
const qryDb = new queryDatabase(client);
qryDb.openConnection();

test("'queryDatabase.write()' function should return the number of inserts", () => {
  return qryDb
    .write("maxima", "Test", [
      { doc: "test1" },
      { doc: "test2" },
      { doc: "test3" },
    ])
    .then((result) => {
      const writeResult = result;
      const control = { ok: 1, n: 3 };
      expect(writeResult).toEqual(control);
    })
    .catch((err) => {
      console.log(err);
    });
});

test("'queryDatabase.read()' function should return an object", () => {
  return qryDb
    .read("maxima", "Test", {})
    .then((result) => {
      const readResult = result;
      const control = [1, 2, 3];
      expect(readResult.length).toEqual(control.length);
    })
    .catch((err) => {
      console.log(err);
    });
});

test("'queryDatabase.update()' function should return the number of updates", () => {
  return qryDb
    .update("maxima", "Test", { $set: { doc: "updateTest" } }, { doc: "test1" })
    .then((result) => {
      const updateResult = result;
      expect(updateResult).toEqual({ n: 1, nModified: 1, ok: 1 });
    })
    .catch((err) => {
      console.log(err);
    });
});

test("'queryDatabase.delete()' function should return the number of deletes", () => {
  return qryDb
    .delete("maxima", "Test", { doc: "test2" })
    .then((result) => {
      const deleteResult = result;
      const control = { n: 1, ok: 1 };
      expect(deleteResult).toEqual(control);
    })
    .catch((err) => {
      console.log(err);
    });
});
