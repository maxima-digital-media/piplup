import React, { useEffect, useState } from "react";

import ItemCard from "../components/ItemCard";
import PreviewCard from "../components/PreviewCard";
import TopBar from "../components/TopBar";
import VersionBadge from "../components/VersionBadge";
import VideoPlayer from "../components/VideoPlayer";

export default function App() {
  const [contentData, setContentdata] = useState(undefined);
  const [previewData, setPreviewData] = useState(undefined);
  const [trailerData, setTrailerData] = useState(undefined);

  let state = undefined;
  const [hostname, setHostname] = useState(state);

  useEffect(() => {
    setHostname(window.location.hostname);
    fetchData(window.location.hostname);
  }, []);

  async function fetchData(ip) {
    // await getData(ip).then((res) => {
    //   setContentdata((prevState) => res);
    // });
    let url = `http://${ip}:8080/data/maxima/piplup/d`
    const response = await fetch(url)
    const data = await response.json()
    setContentdata((prevState) => data); 
  }

  function itemClicked(id) {
    return contentData.forEach((item, index) => {
      if (item._id === id) {
        setPreviewData((prevSate) => item);
      }
    });
  }

  function playTrailer() {
    destroyPreviewCard();
    setTrailerData((prevSate) => previewData);
  }

  function renderItemCards() {
    return contentData.map((data) => (
      <li key={data._id}>
        <ItemCard
          itemId={data._id}
          onItemClicked={itemClicked}
          img={data.poster_path}
          title={data.title}
        ></ItemCard>
      </li>
    ));
  }

  function destroyPreviewCard() {
    setPreviewData((prevSate) => undefined);
  }

  function destroyPlayer() {
    setPreviewData((prevSate) => trailerData);
    setTrailerData((prevSate) => undefined);
  }

  function renderPreviewCard() {
    return (
      <div>
        <div
          onClick={destroyPreviewCard}
          className="fixed top-0 z-10 w-screen h-screen bg-black opacity-80 cursor-pointer"
        ></div>
        <PreviewCard
          data={previewData}
          onDestroy={destroyPreviewCard}
          onPlayTrailer={playTrailer}
        ></PreviewCard>
      </div>
    );
  }

  function renderPlayer() {
    return (
      <div>
        <div
          onClick={destroyPlayer}
          className="fixed top-0 z-10 w-screen h-screen bg-black opacity-80 cursor-pointer"
        ></div>
        <VideoPlayer data={trailerData} onDestroy={destroyPlayer}></VideoPlayer>
      </div>
    );
  }

  return (
    <div>
      <TopBar></TopBar>
      {previewData !== undefined && renderPreviewCard()}
      {
        <ul className="grid grid-cols-2 gap-2 m-3.5">
          {contentData !== undefined && renderItemCards()}
        </ul>
      }
      {trailerData !== undefined && renderPlayer()}
      <VersionBadge></VersionBadge>
    </div>
  );
}

// async function getData(ip) {
//   const cacheVersion = 0.1;
//   const cacheName = `piplup-v${cacheVersion}-data`;
//   const url = `http://${ip}:8080/data/maxima/piplup/d`;
//   let cachedData = await getCachedData(cacheName, url);

//   if (cachedData) {
//     return cachedData;
//   } else {
//     const cacheStorage = await caches.open(cacheName);
//     await cacheStorage.add(url);
//     cachedData = await getCachedData(cacheName, url);
//     await deleteOldCaches(cacheName);
//   }

//   return cachedData;
// }

// // Get data from the cache.
// async function getCachedData(cacheName, url) {
//   const cacheStorage = await caches.open(cacheName);
//   const cachedResponse = await cacheStorage.match(url);

//   if (!cachedResponse || !cachedResponse.ok) {
//     return false;
//   }

//   return await cachedResponse.json();
// }

// // Delete any old caches to respect user's disk space.
// async function deleteOldCaches(currentCache) {
//   const keys = await caches.keys();

//   for (const key of keys) {
//     const isOurCache = "piplup-" === key.substr(0, 7);

//     if (currentCache === key || !isOurCache) {
//       continue;
//     }

//     caches.delete(key);
//   }
// }
