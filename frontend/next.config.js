module.exports = {
  excludeFile: (str) => /\*.{stories,spec,test}.js/.test(str)
}
