import React, { useState, useEffect, useRef } from "react";

export default function VideoPlayer(props) {
  const [hostname, setHostname] = useState(undefined);
  const [videoState, setVideoState] = useState(undefined);

  useEffect(() => {
    setHostname(window.location.hostname);
  }, []);

  useEffect(() => {
    setVideoState({
      source: props.data.trailer,
      poster: `http://${hostname}:8080/media/image${props.data.backdrop_path}`,
      paused: true,
      currentTime: null,
      duration: null,
      fullscreen: false,
      volume: 1,
    });
  }, [hostname]);

  const [controlVisibility, setControlVisibility] = useState({
    visibility: "visible",
  });
  const [currTime, setCurrTime] = useState(0);

  const videoElm = useRef(null);

  // useEffect(() => {
  //   setCurrTime(videoElm.currentTime);
  // }, []);

  function togglePlay() {
    const method = videoState.paused ? "play" : "pause";
    const video = document.getElementById("video");

    if (videoState.paused === false) {
      setControlVisibility({ visibility: "visible" });

      setVideoState((prevState) => {
        let newState = prevState;
        newState.paused = true;
        return newState;
      });
    } else if (videoState.paused === true) {
      setControlVisibility({ visibility: "hidden" });

      setVideoState((prevState) => {
        let newState = prevState;
        newState.paused = false;
        return newState;
      });
    }

    video[method]();
  }

  function toggleFullscreen() {
    const player = document.getElementById("video-player");

    if (videoState.fullscreen === false) {
      openFullscreen();
      setVideoState((prevState) => {
        let newState = prevState;
        newState.fullscreen = true;
        return newState;
      });
    } else if (videoState.fullscreen === true) {
      closeFullscreen();
      setVideoState((prevState) => {
        let newState = prevState;
        newState.fullscreen = false;
        return newState;
      });
    }

    /* Open fullscreen */
    function openFullscreen() {
      if (player.requestFullscreen) {
        player.requestFullscreen();
      } else if (player.webkitRequestFullscreen) {
        /* Safari */
        player.webkitRequestFullscreen();
      } else if (player.msRequestFullscreen) {
        /* IE11 */
        player.msRequestFullscreen();
      }
    }

    /* Close fullscreen */
    function closeFullscreen() {
      if (document.exitFullscreen) {
        document.exitFullscreen();
      } else if (document.webkitExitFullscreen) {
        /* Safari */
        document.webkitExitFullscreen();
      } else if (document.msExitFullscreen) {
        /* IE11 */
        document.msExitFullscreen();
      }
    }
  }

  return (
    <div
      id="video-player"
      className="fixed z-20 left-0 top-60 m-4 cursor-pointer"
    >
      <div className="fixed left-0 top-60 m-4 h-auto bg-gray-700">
        {videoState !== undefined && (
          <video
            onClick={togglePlay}
            ref={videoElm}
            id="video"
            className="relative w-full h-full"
            poster={videoState.poster}
          >
            <source
              src={`http://${hostname}:8080/media/video/${videoState.source}.mp4`}
            ></source>
          </video>
        )}
        <div
          id="player-controls"
          className="absolute top-0 w-full h-full"
          style={controlVisibility}
        >
          <div
            onClick={togglePlay}
            id="pp-btn"
            className="flex justify-center items-center w-full h-full cursor-pointer"
          >
            <svg
              xmlns="http://www.w3.org/2000/svg"
              className="h-14"
              viewBox="0 0 20 20"
              fill="#1E40AF"
            >
              <path
                fillRule="evenodd"
                d="M10 18a8 8 0 100-16 8 8 0 000 16zM9.555 7.168A1 1 0 008 8v4a1 1 0 001.555.832l3-2a1 1 0 000-1.664l-3-2z"
                clipRule="evenodd"
              />
            </svg>
            {/*<svg
              xmlns="http://www.w3.org/2000/svg"
              viewBox="0 0 20 20"
              fill="#1E40AF"
            >
              <path
                  fillRule="evenodd"
                d="M18 10a8 8 0 11-16 0 8 8 0 0116 0zM7 8a1 1 0 012 0v4a1 1 0 11-2 0V8zm5-1a1 1 0 00-1 1v4a1 1 0 102 0V8a1 1 0 00-1-1z"
                clipRule="evenodd"
              />
            </svg>*/}
          </div>
          {/*<div className="relative w-full h-1/6 grid grid-cols-8 grid-rows-2">
            <div className="col-span-7 row-span-2 col-start-1 h-full">
              <span className="font-medium text-sm text-white pl-4">
                {Math.floor(currTime/60)}:{currTime.toString().slice(0,2)} -
                {Math.floor(videoElm.current.duration/60)}:{videoElm.current.duration.toString().slice(0,2)}
              </span>
              <div id="progress-bar" className="bg-blue-800 mt-1 h-2 rounded mx-2"></div>
            </div>
            <div onClick={toggleFullscreen} id="toggle-fullscreen-btn" className="flex justify-center items-center row-span-full col-start-8 cursor-pointer">
              <svg xmlns="http://www.w3.org/2000/svg" className="h-6 w-6" color="white" viewBox="0 0 24 24" stroke="currentColor">
                <path strokeLinecap="round" strokeLinejoin="round" strokeWidth="2" d="M4 8V4m0 0h4M4 4l5 5m11-1V4m0 0h-4m4 0l-5 5M4 16v4m0 0h4m-4 0l5-5m11 5l-5-5m5 5v-4m0 4h-4" />
              </svg>
            </div>
          </div>*/}
        </div>
      </div>
    </div>
  );
}
