import React, {useState} from "react";

function VersionBadge() {
  const styles = {
    backgroundColor: "#424242",
  };

  const [easterEggState, setEasterEggState ] = useState(false)

  function versionBadgeClicked() {
    easterEggState? setEasterEggState(false) : setEasterEggState(true)
  }

  return (
    <div
      onClick={versionBadgeClicked}
      className="fixed bottom-0 left-0 rounded-md rounded-l-none rounded-b-none p-2 w-auto text-white cursor-pointer"
      style={styles}
    >
    { easterEggState === true &&
      <div onClick={versionBadgeClicked}
        className="fixed z-10 top-0 left-0 w-full h-screen p-2 bg-black bg-opacity-80 flex flex-col justify-center content-center cursor-pointer">
      <img
        className="object-contain"
        src="piplup.gif"></img>
      <h3 className="flex justify-center m-2"> Project Piplup </h3>
    </div> }
      <span>piplup v0.1.0</span>
    </div>
  );
}

export default VersionBadge;
