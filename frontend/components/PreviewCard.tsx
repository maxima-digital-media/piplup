import React, { useEffect, useState } from "react";

function PreviewCard(props) {
  let state: any = undefined;
  const [hostname, setHostname] = useState(state);

  useEffect(() => {
    setHostname(window.location.hostname);
  }, []);

  const imgStyles = {
    filter: "brightness(60%)",
  };
  const cardStyles = {
    backgroundColor: "#424242",
  };

  // function downloadFile(event) {
  //   const link: HTMLElement | HTMLAnchorElement | null = document.getElementById("downloadBtn");
  //   if (link instanceof HTMLAnchorElement){
  //     const url = link.href;
  //     event.preventDefault();
  //
  //     fetch(url)
  //       .then((res) => {
  //         const file = window.URL.createObjectURL(new Blob([res.data]));
  //         window.URL.revokeObjectURL(file);
  //       })
  //       .catch((err) => {
  //         console.log(err);
  //     });
  //   }
  // }

  return (
    <div
      className="fixed left-2 top-60 h-auto w-11/12 rounded-md overflow-hidden z-20"
      style={cardStyles}
    >
      <div>
        {/* Backdrop  */}
        <img
          className="object-cover"
          style={imgStyles}
          src={`http://${hostname}:8080/media/image${props.data.backdrop_path}`}
          alt="backdrop"
        ></img>
      </div>

      <div className="absolute top-0 text-sm text-white m-3">
        <div className="p-2">
          <div className="flex justify-between">
            {/* Title | Rating | Duration */}
            <div>
              <div className="grid grid-cols-4 gap-1 text-ms border-b-2 pb-1">
                <span className="col-span-3 pl-2">{props.data.title}</span>
                <span className="col-span-1">
                  {props.data.vote_average} / 10
                </span>
                {/*<span className="col-span-1">130 mins</span>*/}
              </div>
            </div>

            {/* Close card button */}
            <div
              onClick={props.onDestroy}
              className="col-span-1 cursor-pointer"
            >
              <div className="w-6 h-6 bg-blue-800 rounded-full flex justify-center">
                <svg
                  className="w-5"
                  fill="none"
                  stroke="currentColor"
                  viewBox="0 0 24 24"
                  xmlns="http://www.w3.org/2000/svg"
                >
                  <path
                    strokeLinecap="round"
                    strokeLinejoin="round"
                    strokeWidth="2"
                    d="M6 18L18 6M6 6l12 12"
                  ></path>
                </svg>
              </div>
            </div>
          </div>

          {/* Item description */}
          <div className="mt-3 h-24 overflow-scroll">
            <p>{props.data.overview}</p>
          </div>
        </div>
      </div>

      {/* Action Buttons  */}
      <div className="flex justify-around m-4">
        <span id="preview-act-btn">
          <a
            id="downloadBtn"
            className="text"
            href={`http://${hostname}:8080/media/video/${props.data.title}.mp4`}
            download={`${props.data.title}.mp4`}
          >
            download
          </a>
        </span>
        <span onClick={props.onPlayTrailer} id="preview-act-btn">
          play trailer
        </span>
      </div>
    </div>
  );
}

export default PreviewCard;
