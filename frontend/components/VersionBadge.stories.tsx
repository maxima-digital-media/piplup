import React from "react";
import VersionBadge from "./VersionBadge";

export default {
  title: "VersionBadge",
  component: VersionBadge,
};

export const Primary = () => <VersionBadge></VersionBadge>;
