import React, { useEffect, useState } from "react";

function ItemCard(props) {
  let state: any = undefined;
  const [hostname, setHostname] = useState(state);

  useEffect(() => {
    setHostname(window.location.hostname);
  });

  function itemClicked(event) {
    props.onItemClicked(props.itemId);
  }
  return (
    <div className="px-3">
      <div
        data-id={props.itemId}
        onClick={itemClicked}
        className="item-card w-36 cursor-pointer"
      >
        <div>
          <img
            className="object-cover"
            src={`http://${hostname}:8080/media/image${props.img}`}
            alt="poster"
          ></img>
        </div>
        <span className="p-2">{props.title}</span>
      </div>
    </div>
  );
}

export default ItemCard;
