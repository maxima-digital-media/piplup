import React from "react";

function TopBar() {
  return (
    <div className="logo-header sticky top-0 z-20">
      <span>M A X I M A</span>
    </div>
  );
}

export default TopBar;
